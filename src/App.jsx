import React from 'react';
import ProductList from './components/ProductList';
import Modal from './components/Modal';

import { faCartShopping } from "@fortawesome/free-solid-svg-icons";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import './App.scss';

class App extends React.Component {
  state = {
    products: [],
    cartItems: JSON.parse(localStorage.getItem('cartItems')) || [],
    favoriteItems: JSON.parse(localStorage.getItem('favoriteItems')) || [],
    showCartModal: false,
    showFavoritesModal: false,
  };

  componentDidMount() {
    fetch('http://localhost:3000/products.json')
    .then(response => response.json())
    .then(products => {
      this.setState({ products });
    })
    .catch(error => {
      console.error('Error fetching products:', error);
    });

  }

  handleAddToCart = (product) => {
    const { cartItems } = this.state;
    const indexItem = cartItems.findIndex(item => item.id === product.id);
    console.log(cartItems);
    if (indexItem === -1) {
      const newItem = {
        id: product.id,
        name: product.name,
        price: product.price,
        image: product.image,
        quantity: 1
      };
      this.setState({ cartItems: [...cartItems, newItem] }, () => {
        localStorage.setItem('cartItems', JSON.stringify(this.state.cartItems));
      });
    } else {
      const updatedItem = {
        ...cartItems[indexItem],
        quantity: cartItems[indexItem].quantity + 1
      };
      const updatedCartItems = [...cartItems];

      updatedCartItems.splice(indexItem, 1, updatedItem);
      this.setState({ cartItems: updatedCartItems }, () => {
        localStorage.setItem('cartItems', JSON.stringify(this.state.cartItems));
      });
    }
  }

  handleRemoveFromCart = (product) => {
    const { cartItems } = this.state;
    const updatedCartItems = [...cartItems];
  
    const indexItem = updatedCartItems.findIndex(item => item.id === product.id);
  
    if (indexItem !== -1) {
      updatedCartItems[indexItem].quantity -= 1;
  
      if (updatedCartItems[indexItem].quantity === 0) {
        updatedCartItems.splice(indexItem, 1);
      }
  
      this.setState({ cartItems: updatedCartItems }, () => {
        localStorage.setItem('cartItems', JSON.stringify(this.state.cartItems));
      });
    }
  };

  handleToggleCartModal = () => {
    this.setState({ showCartModal: !this.state.showCartModal });
  }

  handleAddToFavorites = (product) => {
    const { favoriteItems } = this.state;
    const indexItem = favoriteItems.findIndex(item => item.id === product.id);

    if (indexItem === -1) {
      const newItem = {
        id: product.id,
        name: product.name,
        price: product.price,
        image: product.image
      };
      this.setState({ favoriteItems: [...favoriteItems, newItem] }, () => {
        localStorage.setItem('favoriteItems', JSON.stringify(this.state.favoriteItems));
      });
    } else {
      const updatedFavoriteItems = favoriteItems.filter(item => item.id !== product.id);
      
      this.setState({ favoriteItems: updatedFavoriteItems }, () => {
        localStorage.setItem('favoriteItems', JSON.stringify(this.state.favoriteItems));
      });
    }
  }

  handleToggleFavoritesModal = () => {
    this.setState({ showFavoritesModal: !this.state.showFavoritesModal });
  }

  handleOutsideClick = (event) => {
    if (event.currentTarget === event.target) {
      this.setState({ 
        showCartModal: false,
        showFavoritesModal: false  
      });
    }
  };
  handleItemCartClose = (id) => {
    const { cartItems } = this.state;
    const updatedCartItems = cartItems.filter(item => item.id !== id);

    this.setState({ cartItems: updatedCartItems }, () => {
      localStorage.setItem('cartItems', JSON.stringify(this.state.cartItems));
    });
  };

  render() {
    const { products, cartItems, favoriteItems, showCartModal, showFavoritesModal } = this.state;
    const cartItemTotal = cartItems.reduce((total, item) => total + item.quantity, 0);
    const totalPrice = cartItems.reduce((total, item) => total + item.price * item.quantity, 0);
    const favoriteItemTotal = favoriteItems.length;

    return (
      <div className="app-wrapper">
        <header className="header">
          <div className="container header-wrapper">
            <h1 className="title">Oригінальні товари з Европи</h1>
            <div className="icons-wrapper">
              <div className="cart-shopping-wrapper" onClick={this.handleToggleCartModal}>
                <FontAwesomeIcon icon={faCartShopping} size="sm" style={{color: cartItems.length ? 'yellow' : 'blue' }} /> <span>{cartItemTotal}</span>
              </div>
              <div className="star-wrapper" onClick={this.handleToggleFavoritesModal}>
                <FontAwesomeIcon icon={faStar} size="sm" style={{outlineColor: "blue", color: favoriteItems.length ? 'yellow' : 'blue' }} /> <span>{favoriteItemTotal}</span>
              </div>
            </div>
          </div>
        </header>
        <ProductList
          products={products}
          cartItems={cartItems}
          favoriteItems={favoriteItems}
          AddToCart={this.handleAddToCart}
          RemoveFromCart={this.handleRemoveFromCart}
          AddToFavorites={this.handleAddToFavorites}
          showCartModal={this.handleToggleCartModal}
          ItemCartClose={this.handleItemCartClose}
        />
        {showCartModal && 
          <Modal
            optionalСlassName="shoping-cart__modal"
            cartItems={cartItems} 
            closeButton={this.handleToggleCartModal}
            closeModal={this.handleToggleCartModal}
            handleOutsideClick={this.handleOutsideClick}
            header="Кошик"
            text={
              !cartItems.length &&
                <div>Ваш кошик порожній.</div>
              }
            actions={cartItems.map(item =>
              <div className="item-wrapper__modal" key={item.id}>
                <div className="item-text__modal">
                  <div className="item-name__modal">
                    {item.name}
                  </div>
                  <div className="item-price__modal">
                  {item.price} грн
                  </div>
                </div>
                <div className="add-remove-btn__wrapper">
                  <button className="btn minus-from-cart" onClick={() => this.handleRemoveFromCart(item)}>-</button>
                  <div className="quantity-product">
                    {item.quantity}
                  </div>
                  <button className="btn plus-to-cart" onClick={() => this.handleAddToCart(item)}>+</button>
                  <button className="btn close-item__modal" onClick={() => this.handleItemCartClose(item.id)}>
                    &times;
                  </button>          
                </div>
              </div>
            )}

            totalPrice={
            cartItems.length &&
              <div className="total-price__modal">Разом: {totalPrice} грн</div>
            }
          />
        }
        {showFavoritesModal && 
          <Modal
            optionalСlassName="favorites__modal" 
            favoriteItems={favoriteItems}
            closeButton={this.handleToggleFavoritesModal}
            closeModal={this.handleToggleFavoritesModal}
            handleOutsideClick={this.handleOutsideClick}
            header="Улюблені товари"
            text={
              !favoriteItems.length &&
                <div>У вас ще немає улюблених товарів.</div>
              }
            actions={favoriteItems.map(item =>
              <div className="item-wrapper__modal" key={item.id}>
                <div className="item-text__modal">
                  <div className="item-name__modal">
                    {item.name}
                  </div>
                  <div className="item-price__modal">
                    {item.price} грн
                  </div>
                </div>
                <button className="btn__favorite-modal" onClick={() => this.handleAddToCart(item)}>Додати до кошика</button>
              </div>
            )}
            totalPrice=""
          />
        }

      </div>
    );
  }
}

export default App;


