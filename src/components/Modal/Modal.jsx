import React from "react";
import PropTypes from 'prop-types';
import "./Modal.scss";



class Modal extends React.Component {
   
    render() {
        const { header, optionalСlassName, closeButton, totalPrice, text, actions, closeModal, handleOutsideClick } = this.props;
        return (
        <div className="modal-wrapper" onClick={handleOutsideClick}>
            <div className={`modal ${optionalСlassName}`}>
                <div className="modal-content">
                    <div className="modal-header">
                        <h2>{header}</h2>
                        {closeButton && (
                        <span className="modal-close" onClick={closeModal}>
                            &times;
                        </span>
                        )}
                    </div>
                    <div className="modal-body">{actions}</div>
                    <p className="body-text">{text}</p>
                    <div className="modal-footer">{totalPrice}</div>
                </div>
            </div>
        </div>
        );
    }
}

Modal.propTypes = {
    header: PropTypes.string, 
    closeButton: PropTypes.bool, 
    text: PropTypes.string, 
    actions: PropTypes.arrayOf(
        PropTypes.string,
        PropTypes.func,
        ), 
    closeModal: PropTypes.bool, 
    handleOutsideClick: PropTypes.bool, 
}

export default Modal;