import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import './ProductCard.scss';

class ProductCard extends React.Component {

  handleClickAddToCart = () => {
    this.props.AddToCart(this.props.product);
  }

  handleClickRemoveFromCart = () => {
    this.props.RemoveFromCart(this.props.product);
  }

  handleClickAddToFavorites = () => {
    this.props.AddToFavorites(this.props.product);
  }

  handleToggleCartModal = () => {
    this.props.showCartModal(this.props.product)
  }

  handleItemCartClose = () => {
    this.props.ItemCartClose(this.props.product.id)
  }

  render() {
    const { name, price, image} = this.props.product;
    const { inCart, inFavorites } = this.props;
    return (
      <div className="product-card">
        <div className="image-wrapper">
          <img src={image} alt={name} />
        </div>
        <h3 className="product-name">{name}</h3>
        
        <div className="card-footer">
          {!inFavorites &&
            <button className="add-favorite" onClick={this.handleClickAddToFavorites}>
              <FontAwesomeIcon icon={faStar} size="xs" style={{color: "blue",}} />
            </button>
          }
          {inFavorites &&
            <button 
              className="add-favorite" 
              style={{backgroundColor: "#dddddd", borderRadius: "60px"}} 
              onClick={this.handleClickAddToFavorites}
              >
              <FontAwesomeIcon icon={faStar} size="xs" style={{color: "yellow"}} />
            </button>
          }
          <p className="product-price">{price} грн</p>
        </div>
        {!inCart &&
          <button className="add-to-cart" onClick={this.handleClickAddToCart}>Додати до кошика</button>
        }
        {inCart &&
          <button className="remove-to-cart" onClick={this.handleItemCartClose}>Товар у кошику</button> 
        }
      </div>
    );
  }
}

ProductCard.propTypes = {
product: PropTypes.object,
inCart: PropTypes.bool,
inFavorites: PropTypes.bool,
AddToCart: PropTypes.func,
RemoveFromCart: PropTypes.func,
AddToFavorites: PropTypes.func
};

export default ProductCard;


